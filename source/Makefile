# ----------------------------------------------------------------------------
#
#  Copyright (C) 2010-2018 Fons Adriaensen <fons@linuxaudio.org>
#    
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------


PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
SHARED ?= $(PREFIX)/share/zita-mu1

VERSION = 0.3.3
CPPFLAGS += -MMD -MP -DVERSION=\"$(VERSION)\" -DSHARED=\"$(SHARED)\"
CXXFLAGS += -O2 -Wall -ffast-math -pthread
CXXFLAGS += -march=native


all:	zita-mu1


ZITA-MU1_O = zita-mu1.o styles.o mainwin.o jclient.o png2img.o button.o rotary.o guiclass.o \
	kmeter.o cmeter.o kmeterdsp.o cmeterdsp.o 

zita-mu1:	CPPFLAGS += $(shell pkgconf --cflags freetype2)
zita-mu1:	LDLIBS += -lclxclient -lclthreads -ljack -lpthread -lcairo -lpng -lXft -lX11 -lrt
zita-mu1:	$(ZITA-MU1_O)
	$(CXX) $(LDFLAGS) -o $@ $(ZITA-MU1_O) $(LDLIBS)

$(ZITA-MU1_O):
-include $(ZITA-MU1_O:%.o=%.d)


install:	all
	install -d $(DESTDIR)$(BINDIR)
	install -d $(DESTDIR)$(SHARED)
	install -m 755 zita-mu1 $(DESTDIR)$(BINDIR)
	install -m 644 ../share/*.png $(DESTDIR)$(SHARED)


uninstall:
	/bin/rm -f  $(DESTDIR)$(BINDIR)/zita-mu1
	/bin/rm -fr  $(DESTDIR)$(SHARED)


clean:
	/bin/rm -f *~ *.o *.a *.d *.so
	/bin/rm -f zita-mu1
