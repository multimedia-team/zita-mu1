// ----------------------------------------------------------------------------
//
//  Copyright (C) 2010-2015 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __JCLIENT_H
#define __JCLIENT_H


#include <inttypes.h>
#include <stdlib.h>
#include <math.h>
#include <clthreads.h>
#include <jack/jack.h>
#include "global.h"
#include "kmeterdsp.h"
#include "cmeterdsp.h"


class DelayAct
{
public:

    enum { OFF, UP, ON, DN };

    DelayAct (void) : _state (OFF) {}
    ~DelayAct (void) {}

    int state (void) const { return _state; }
    int state (bool on)
    {
        if (on)
        {
            switch (_state)
	    {
	    case OFF:
	    case DN: _state = UP; break;
	    case UP: _state = ON; break;
	    }
	}
	else
	{
  	    switch (_state)
            {
            case UP:
            case ON: _state = DN; break;
	    case DN: _state = OFF; break;
	    }
	}
	return _state;
    }

private:
    
    int _state;
};


class Jclient : public A_thread
{
public:

    Jclient (const char *jname, const char *jserv);
    ~Jclient (void);

    const char *jname (void) const { return _jname; }

    void set_inpsel (int m) { _inpsel = m; }
    void set_monmod (int m) { _monmod = m; }
    void set_outsel (int m) { _outsel = m; }
    void set_talkbk (int m) { _talkbk = m; }
    void set_out1gain (float v) { _out1gain = (v < -100) ? 0 : powf (10.0f, 0.05f * v); }
    void set_out2gain (float v) { _out2gain = (v < -100) ? 0 : powf (10.0f, 0.05f * v); }
    void set_out2hdph (bool v) { _out2hdph = v; }
    void set_tb1gain (float v) { _tb1gain = (v < -100) ? 0 : powf (10.0f, 0.05f * v); }
    void set_tb2gain (float v) { _tb2gain = (v < -100) ? 0 : powf (10.0f, 0.05f * v); }

    void  read_kmeter (int i, float *rms, float *dpk) { _kmdsp [i].read (rms, dpk); }
    float read_cmeter (void) { return _cmdsp.read (); }

private:

    void  init_jack (const char *jname, const char *jserv);
    void  close_jack (void);
    void  jack_shutdown (void);
    int   jack_process (int frames);

    int  prepare (int frames);

    virtual void thr_main (void) {}

    jack_client_t  *_jack_client;
    jack_port_t    *_moninpport [8];
    jack_port_t    *_seloutport [2];
    jack_port_t    *_monoutport [4];
    jack_port_t    *_tbinpport [2];
    jack_port_t    *_tboutport [2];
    bool            _active;
    const char     *_jname;
    int             _fsamp;
    int             _bsize;

    int             _fragm;
    int             _frcnt;
    int             _inpsel;
    int             _monmod;
    int             _outsel;
    int             _talkbk;
    float           _out1gain;
    float           _out2gain;
    bool            _out2hdph;
    float           _tb1gain;
    float           _tb2gain;

    Kmeterdsp       _kmdsp [2];
    Cmeterdsp       _cmdsp;
    DelayAct        _inpstat [4];
    DelayAct        _dimstat;
    float           _ipg [4];
    float           _opg [2];
    float           _dog [2];
    float           _tbg [2];
    float           _ww, _zs, _zd;

    static void jack_static_shutdown (void *arg);
    static int  jack_static_process (jack_nframes_t nframes, void *arg);
};


#endif
