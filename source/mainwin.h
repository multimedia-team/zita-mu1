// ----------------------------------------------------------------------------
//
//  Copyright (C) 2010-2015 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __MAINWIN_H
#define	__MAINWIN_H


#include <clxclient.h>
#include "global.h"
#include "jclient.h"
#include "guiclass.h"
#include "kmeter.h"
#include "cmeter.h"


class Mainwin : public A_thread, public X_window, public X_callback
{
public:

    enum { XSIZE = 980, YSIZE = 90, DISP_XS = 510, DISP_YS = 85 };

    Mainwin (X_rootwin *parent, X_resman *xres, Jclient *jclient);
    ~Mainwin (void);
    Mainwin (const Mainwin&);
    Mainwin& operator=(const Mainwin&);

    void stop (void) { _stop = true; }
    int  process (void);

private:

    virtual void thr_main (void) {}

    void handle_time (void);
    void handle_stop (void);
    void handle_event (XEvent *);
    void handle_callb (int type, X_window *W, XEvent *E);
    void handle_inpsel (int e, int k, int b);
    void handle_monmod (int e, int k, int b);
    void handle_outsel (int e, int k, int b);
    void handle_talkbk (int e, int k, int b);
    void disp_inpsel (void);
    void disp_monmod (void);
    void disp_outsel (void);
    void disp_talkbk (void);
    void update1 (int k);
    void update2 (void);

    void expose (XExposeEvent *E);
    void clmesg (XClientMessageEvent *E);
    void redraw_main (void);
    void redraw_disp (void);
    void makegui (void);
    void addtext (X_window *W, X_textln_style *T, int xp, int yp, int xs, int ys, const char *text, int align);
    XImage *loadpng (const char *file);

    enum
    {
	B_INP1, B_INP2, B_INP3, B_INP4,
        B_LEFT, B_RITE, B_MONO, B_DIMM,
	B_OUT1, B_OUT2, B_HDPH,
        B_TKB1, B_TKB2,
        N_BUTT, B_PRES
    };

    enum
    {
	R_VOL1, R_VOL2, R_TB1, R_TB2,
        N_RCTL
    };

    X_resman       *_xres;
    Atom            _atom;
    bool            _stop;
    Jclient        *_jclient;
    PushButton     *_buttons [N_BUTT];
    int             _inpsel;
    int             _monmod;
    int             _outsel;
    int             _talkbk;
    RotaryGeom      _volgeom;
    Rlevel100A     *_rvolume [2];
    RotaryGeom      _tb1geom;
    Rlevel100A     *_rtblev1;
    RotaryGeom      _tb2geom;
    Rlevel100A     *_rtblev2;
    X_subwin       *_dispwin;
    Kmeter         *_meters [2];
    Cmeter         *_cmeter;
    X_tbutton      *_pkbutt;
    float           _peak1;
    float           _peak2;
};


#endif
