// ----------------------------------------------------------------------------
//
//  Copyright (C) 2010-2017 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include "styles.h"


XftColor        *XftColors [NXFTCOLORS];
XftFont         *XftFonts [NXFTFONTS];
X_button_style   Bst0;
X_textln_style   Tst0;


void styles_init (X_display *disp, X_resman *xrm)
{
    XftColors [C_MAIN_BG]  = disp->alloc_xftcolor (0.2f, 0.2f, 0.2f, 1.0f);
    XftColors [C_MAIN_FG]  = disp->alloc_xftcolor (1.0f, 1.0f, 1.0f, 1.0f);
    XftColors [C_MAIN_LS]  = disp->alloc_xftcolor (0.5f, 0.5f, 0.5f, 1.0f);
    XftColors [C_MAIN_DS]  = disp->alloc_xftcolor (0.0f, 0.0f, 0.0f, 1.0f);
    XftColors [C_DISP_BG]  = disp->alloc_xftcolor (0.0f, 0.0f, 0.0f, 1.0f);

    XftFonts [F_TEXT] = disp->alloc_xftfont (xrm->get (".font.text",  "helvetica:bold:pixelsize=11"));
    XftFonts [F_JKMP] = disp->alloc_xftfont (xrm->get (".font.butt", "luxi:bold:pixelsize=10"));

    Bst0.size.x = 30;
    Bst0.size.y = 16;
    Bst0.font = XftFonts [F_JKMP];
    Bst0.type = X_button_style::PLAIN;
    Bst0.color.bg[0] = disp->alloc_color ("#000000", 0);
    Bst0.color.fg[0] = disp->alloc_xftcolor ("#8080ff", 0);
    Bst0.color.bg[1] = disp->alloc_color ("#000000", 0);
    Bst0.color.fg[1] = disp->alloc_xftcolor ("#40ff40", 0);
    Bst0.color.bg[2] = disp->alloc_color ("#000000", 0);
    Bst0.color.fg[2] = disp->alloc_xftcolor ("#ffff00", 0);
    Bst0.color.bg[3] = disp->alloc_color ("#ff2000", 0);
    Bst0.color.fg[3] = disp->alloc_xftcolor ("#ffffff", 0);

    Tst0.font = XftFonts [F_TEXT];
    Tst0.color.normal.bgnd = XftColors [C_MAIN_BG]->pixel;
    Tst0.color.normal.text = XftColors [C_MAIN_FG];
}


void styles_fini (X_display *disp)
{
}
